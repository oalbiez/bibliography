# Bibliography


## Psychologie / Neuroscience

- La vérité sur ce qui nous motive. Daniel Pink , Isaac Getz, et al.
- Flow: The Classic Work on How to Achieve Happiness, with a new Introduction. Mihaly Csikszentmihalyi.
- System 1 / système 2 : les deux vitesses de la pensée. Daniel Kahneman.
- Software for your Head. Jim McCarthy.


## Management

- Turn The Ship Around!: A True Story of Building Leaders by Breaking the Rules. David Marquet.
- Organizational Innovation by Integrating Simplification: Learning from Buurtzorg Nederland. Nandram, Sharda S.
- Joy, Inc.: How We Built a Workplace People Love. Sheridan, Richard
- L'auto-organisation, ça marche ! Vermeer, Astrid.
- The Ideal Team Player: How to Recognize and Cultivate the Three Essential Virtues. Lencioni, Patrick M.
- How to Change the World: Change Management 3.0. Appelo, Jurgen
- Optimisez votre équipe Broché – 1 janvier 2005. Patrick Lencioni.
- Reinventing Organizations: Vers des communautés de travail inspirées. Frédéric Laloux.
- Le management lean. Michael Ballé.
- Qui a piqué mon fromage ? Specer Johnson.
- The Responsibility Virus. Roger Martin.
- Le management selon Shiba. Shoji Shiba.
- Objectif zéro sale con. Robert Sutton.


## Production / Lean

- Le But : Un processus de progrès permanent. Eliyahus M. Goldratt.
- Le Gold Mine, un récit lean. Freddy Ballé et Michael Ballé.
- The Toyota Way. Jeffrey K. Liker.


## Coaching

- Le thérapeute et le philosophe : Atteindre un but par le non-agir. Gerbinet Dany, Emmanuelle Piquet, et al.
- Solution Focus : coaching, leadership, conversations constructives. Géry Derbier
- Coaching Plain and Simple – Solution–Focused Brief Coaching Essentials. Peter Szabó, Kirsten Dierolf, Daniel Meier.
- Manuel pratique de Thérapie Orientée Solution : Dialogues et récits. Vallée, Alain
- Brief Coaching: A Solution Focused Approach. Iveson, Chris.
- Du désir au plaisir de changer - Le coaching du changement. Kourilsky, Françoise.
- The Solution Focused Marriage: 5 Simple Habits That Will Bring Out the Best in Your Relationship. Connie, Elliott.
- Solution Focused Brief Therapy: 100 Key Points and Techniques. Ratner, Harvey.
- Solution Building in Couples Therapy. Connie, Elliott.
- Au delà des miracles. Steve de Shazer.


## Philosophie

- Introduction à la pensée complexe. Morin, Edgar
- Thinking in Systems. Donella Meadows.
- Les décisions absurdes. Christian Morel.
- Le Zen et le traité des motocyclettes. Robert Pirsig.
- Comment faire tomber un dictateur quand on est seul tout petit et sans arme. Srdja Popovic.
- La juste part. David Robichaud.
- Théorie U. Otto Scharmer.
- La cinquième discipline. Peter Senge.
- Le cygne noir. Nicholas Taleb.
- Antifragile: les bienfaits du désordre. Nicholas Taleb.


## CNV

- Les mots sont des fenêtres (ou bien ce sont des murs). Marshall Rosenberg.
- La communication non violente au quotidien. Marshall Rosenberg.
- Pratiquer la CNV au travail - 2e éd. - La communication NonViolente: La communication NonViolente, passeport pour réconcilier bien être et performance. Keller, Françoise


## Agilité

- Le story mapping - Visualisez vos user stories pour développer le bon produit. Jeff Patton.
- Scrum and Xp from the Trenches 2nd Edition. Henrik Kniberg.
- Running lean. Ash Maurya.
- Lean startup. Eric Ries.
- Stop starting, start finishing. Arne Roock.




## Software

- The Principles of Product Development Flow: Second Generation Lean Product Development. Reinertsen, Donald G.


## Divers

- Une fabrique de libertés : Le Lycée autogéré de Paris. Patrick Boumard.
- Godin, inventeur de l'économie sociale : Mutualiser, coopérer, s'associer. Jean-François Draperi.
- Faire des hommes libres : Boimondau et les Communautés de travail à Valence, 1941-1982. Michel Chaudy.
- Sous le feu, Michel Goya.


# Jeux

- Jeux cadres de Thiagi, Bruno Hourst.
- Jeux de coopération pour les formateurs. François Paul-Cavallier.
